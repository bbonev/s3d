s3d (0.2.2.1-5) unstable; urgency=medium

  * debian/control:
    - Upgraded to policy 4.6.2, no changes required

 -- Sven Eckelmann <sven@narfation.org>  Sat, 31 Dec 2022 17:40:44 +0100

s3d (0.2.2.1-4) unstable; urgency=medium

  [ Sven Eckelmann ]
  * debian/control
    - Switch to correct DEP-14 unstable branch name
    - Upgraded to policy 4.6.1, no changes required
    - Remove versioned constraints unnecessary since buster
    - Replace Build-Depends libfreetype6-dev with libfreetype-dev
  * debian/copyright:
    - Correctly order globbing pattern entries
    - Update copyright years to 2022

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on cmake.
    + s3dvt: Drop versioned constraint on bash in Depends.

 -- Sven Eckelmann <sven@narfation.org>  Mon, 14 Nov 2022 18:36:11 +0100

s3d (0.2.2.1-3) unstable; urgency=medium

  * Drop duplicated fields from upstream/metadata
  * Update debian/watch for format 4
  * debian/rules:
    - Drop default flag --as-needed from linker flags
  * debian/control:
    - Switch to debhelper compat 13
    - Upgraded to policy 4.5.1, no changes required
  * debian/patches:
    - Add s3dosm-Add-support-for-libgps-API-10.patch,
      Add support for libgps API 10 (Closes: #979254)
  * Drop overrides for legacy lintian warning about missing upstream
    changelog

 -- Sven Eckelmann <sven@narfation.org>  Mon, 04 Jan 2021 18:45:47 +0100

s3d (0.2.2.1-2) unstable; urgency=medium

  * debian/patches:
    - Add cmake-Switch-to-external-SDL2-configuration-file.patch,
      Switch to external SDL2 configuration file (Closes: #951929)

 -- Sven Eckelmann <sven@narfation.org>  Sun, 23 Feb 2020 09:41:00 +0100

s3d (0.2.2.1-1) unstable; urgency=medium

  * New Upstream Version
  * debian/control:
    - Upgraded to policy 4.5.0, no changes required
    - Switch to SDL2 and glvn dispatcher
    - depend on cmake 3.9
  * debian/copyright:
    - Update copyright information for 0.2.2.1 release
  * debian/patches:
    - Drop upstream merged
      Add-support-for-TT-instead-of-HNA.patch,
      Add-support-for-libgps-API-5.patch, Fix-FTBFS-glibc2.30.patch,
      Update-author-addresses.patch, s3dosm-Add-support-for-libgps-API-7.patch,
      s3dosm-Add-support-for-libgps-API-9.patch,
      s3dosm-Only-print-STATUS_DGPS_FIX-status-when-it-is-defin.patch,
      s3dvt-Fail-fork-when-setuid-or-setgid-fail.patch
  * debian/*.symbols:
    - Use versioned symbols

 -- Sven Eckelmann <sven@narfation.org>  Sun, 26 Jan 2020 11:04:40 +0100

s3d (0.2.2-20) unstable; urgency=medium

  [ Sven Eckelmann ]
  * debian/control:
    - Upgraded to policy 4.4.1, no changes required
    - Switch debhelper dependency to debhelper-compat
    - Allow build without (fake)root
    - Move repository to from ecsv-guest to ecsv salsa user
  * debian/copyright
    - Update copyright years to 2020
  * debian/patches:
    - Add s3dosm-Add-support-for-libgps-API-9.patch, Add support for
      timespec_t in libgps API 9

  [ Christian Ehrhardt ]
  * d/p/Fix-FTBFS-glibc2.30.patch: avoid issues with stropts/isastream in
    glibc >=2.30 (LP: #1855995)

 -- Sven Eckelmann <sven@narfation.org>  Wed, 15 Jan 2020 21:24:30 +0100

s3d (0.2.2-19) unstable; urgency=medium

  * Upload to unstable

 -- Sven Eckelmann <sven@narfation.org>  Sat, 06 Jul 2019 15:41:02 +0200

s3d (0.2.2-18) experimental; urgency=medium

  * debian/control:
    - Automatically select correct Vcs-Git branch
  * debian/patches:
    - Add s3dosm-Add-support-for-libgps-API-7.patch, Add support for
      libgps API 7 (Closes: #926519)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 06 Apr 2019 16:15:25 +0200

s3d (0.2.2-17) experimental; urgency=medium

  * debian/control
    - Upgraded to policy 4.3.0, no changes required
    - Switch to debhelper 12
  * debian/copyright
    - Update copyright years to 2019

 -- Sven Eckelmann <sven@narfation.org>  Sat, 02 Mar 2019 20:17:11 +0100

s3d (0.2.2-16) unstable; urgency=medium

  * debian/control:
    - Add Multi-Arch tags to packages without maintainer scripts and equal
      common files

 -- Sven Eckelmann <sven@narfation.org>  Sat, 20 Oct 2018 07:35:49 +0200

s3d (0.2.2-15) unstable; urgency=medium

  * Override the lintian warning about missing upstream changelog
  * Override the lintian warning about unfortified functions
  * debian/patches:
    - Add Fix-spelling-errors.patch, Fix spelling errors
  * Change debian URLs to https://
  * debian/upstream/signing-key.asc
    - Import new upstream RSA 4096 key 2DE9541A85CC87D5D9836D5E0C8A47A2ABD72DF9
  * Sort debian control files with `wrap-and-sort -abst`
  * Upgraded to policy 4.2.1
    - remove get-orig-source rule from debian/rules
  * debian/copyright:
    - Update copyright years
  * debian/control:
    - update to debhelper 10
    - Change Vcs-Git and Vcs-Browser to salsa.debian.org
  * debian/rules
    - Remove ddeb migration conflict against pre-stretch package
    - Drop (now default) parameter --parallel for dch
  * debian/watch:
    - Work around incorrect s3d-doc match due to unexpected
      https://qa.debian.org/watch/sf.php/s3d/ file listing

 -- Sven Eckelmann <sven@narfation.org>  Thu, 18 Oct 2018 21:06:06 +0200

s3d (0.2.2-14) unstable; urgency=medium

  * debian/control, debian/rules:
    - Drop s3d-dbg in favor of -dbgsym package
  * debian/patches:
    - Add Documentation-Force-xmlto-encoding-to-UTF-8.patch,
      Force xmlto encoding to UTF-8

 -- Sven Eckelmann <sven@narfation.org>  Wed, 30 Dec 2015 22:27:05 +0100

s3d (0.2.2-13) unstable; urgency=medium

  * Upload to unstable

 -- Sven Eckelmann <sven@narfation.org>  Sun, 01 Nov 2015 11:56:18 +0100

s3d (0.2.2-12) experimental; urgency=medium

  * debian/patches:
    - Move patches to alternative DEP-3 format and manage them with gbp pq
    - Add s3dosm-Only-print-STATUS_DGPS_FIX-status-when-it-is-defin.patch,
      s3dosm: Only print STATUS_DGPS_FIX status when it is defined

 -- Sven Eckelmann <sven@narfation.org>  Sun, 01 Nov 2015 11:51:01 +0100

s3d (0.2.2-11) experimental; urgency=medium

  * Upgraded to policy 3.9.6, no changes required
  * Update years in debian/copyright
  * debian/patches:
    - Update setuidgid_fail.patch, Add missing CVE Co-Author Ismael Ripoll
      <iripoll@disca.upv.es>, Add missing setuid/setgid check in second
      function (CVE-2014-1226)

 -- Sven Eckelmann <sven@narfation.org>  Thu, 19 Mar 2015 20:28:22 +0100

s3d (0.2.2-10) unstable; urgency=medium

  * Update copyright years in debian/copyright
  * Move keyring for signature verification to new debian/upstream/
  * debian/patches:
    - Extend DEP-3 information in patches
  * debian/rules:
    - Use Largefile Support enabled C API

 -- Sven Eckelmann <sven@narfation.org>  Sat, 30 Aug 2014 16:05:39 +0200

s3d (0.2.2-9) unstable; urgency=low

  * Upgraded to policy 3.9.5, no changes required
  * Remove obsolete DM-Upload-Allowed in debian/control
  * debian/watch:
    - Verify new upstream versions using GPG key 96E5AF383F7C593B6B16
      and 2FE5EC371482956781AF
  * Update e-mail address of Simon Wunderlich
  * debian/copyright:
    - Update e-mail address of Marek Lindner
    - Update e-mail address of Andreas Langer
  * debian/copyright:
    - Add email_addresses.patch, Update author addresses
  * debian/patches:
    - Add setuidgid_fail.patch, Fail fork when setuid or setgid fail

 -- Sven Eckelmann <sven@narfation.org>  Mon, 25 Nov 2013 22:48:25 +0100

s3d (0.2.2-8) unstable; urgency=low

  * Update copyright years in debian/copyright
  * Upgrade debhelper compat to v9
  * Enable all hardening flags in debian/rules
  * Upgraded to policy 3.9.3, no changes required
  * Let debhelper set the buildflags implicitly

 -- Sven Eckelmann <sven@narfation.org>  Sat, 28 Apr 2012 13:16:52 +0200

s3d (0.2.2-7) unstable; urgency=low

  * Install missing man page mcp_object(9) through s3d-doc
  * debian/patches:
    - Add s3dosm_libgps_api5.patch, Add support for libgps API 5
      (Closes: #648497)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 12 Nov 2011 12:51:52 +0100

s3d (0.2.2-6) unstable; urgency=low

  * Add optional and required dependencies to -dev packages
  * Mark all targets in debian/rules as phony
  * Update URLs to new upstream home at sourceforge.net
  * Remove hardening-includes which are now integrated in dpkg-
    buildflags
  * Use debian packaging manual URL as format identifier in
    debian/copyright

 -- Sven Eckelmann <sven@narfation.org>  Mon, 31 Oct 2011 11:35:02 +0100

s3d (0.2.2-5) unstable; urgency=low

  * Build-Depend on cmake >= 2.8.4+dfsg.1-5 to fix FTBFS on armel and
    kfreebsd-*

 -- Sven Eckelmann <sven@narfation.org>  Sat, 11 Jun 2011 22:55:57 +0200

s3d (0.2.2-4) unstable; urgency=low

  * debian/patches:
    - Add meshs3d_tt.patch, Add support for TT instead of HNA
  * Convert libs3d(w)2 to multiarch (Closes: #623767)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 11 Jun 2011 16:54:46 +0200

s3d (0.2.2-3) unstable; urgency=low

  * Build against libsdl1.2-dev instead of libsdl-1.2-dev (Closes:
    #625640)

 -- Sven Eckelmann <sven@narfation.org>  Wed, 04 May 2011 20:56:58 +0200

s3d (0.2.2-2) unstable; urgency=low

  * Don't install application manpages through s3d-doc (Closes: #625489)

 -- Sven Eckelmann <sven@narfation.org>  Tue, 03 May 2011 22:13:04 +0200

s3d (0.2.2-1) unstable; urgency=low

  * New upstream release (Closes: #534628)
  * Upgraded to policy 3.9.2, no changes required
  * debian/patches:
    - Remove upstream merged filter_pie.patch, libgps_2_90.patch,
      libs3d_small-infinity.patch, s3dfm_overflow.patch,
      s3dvt_glibc-pty.patch, version_number.patch
    - Rebase installable-examples.patch
  * Replace manpages with versions generated during build of upstream
    sources
  * Update copyright years in debian/copyright

 -- Sven Eckelmann <sven@narfation.org>  Sun, 01 May 2011 13:36:26 +0200

s3d (0.2.1.1-7) unstable; urgency=low

  * Upload to unstable
  * Updated my maintainer e-mail address
  * Keep dependencies on separate lines in debian/control
  * debian/copyright:
    - Update to DEP5 revision 164
    - Update copyright years

 -- Sven Eckelmann <sven@narfation.org>  Sun, 06 Feb 2011 12:30:01 +0100

s3d (0.2.1.1-6) experimental; urgency=low

  * Upgraded to policy 3.9.1, no changes required
  * Upgrade debhelper compat to v8
  * set *FLAGS using dpkg-buildflags in debian/rules to work like
    dpkg-buildpackage when called directly
  * Use hardening-includes for CFLAGS and LDFLAGS hardened builds
  * Add full BSD-3 license to debian/copyright
  * debian/patches:
    - Add filter_pie.patch, Filter -fPIE and -pie for libs3d(w)
  * Explicit depend build depend on pkg-config

 -- Sven Eckelmann <sven@narfation.org>  Thu, 23 Sep 2010 13:39:58 +0200

s3d (0.2.1.1-5) unstable; urgency=low

  * Correct spelling errors found by lintian
  * debian/copyright: Update copyright years
  * Upgraded to policy 3.8.4, no changes required
  * debian/rules:
    - Enable parallel builds using dh's --parallel
    - Inform about missing installed files using dh's --list-missing
  * debian/patches:
    - Add s3dfm_overflow.patch, Fix potential buffer overflow when
      parsing directory
    - Add libs3d_small-infinity.patch, Don't accidently use INFINITY
      from math.h
    - Add s3dvt_glibc-pty.patch, Reimplement PTY/TTY mode using glibc

 -- Sven Eckelmann <sven@narfation.org>  Fri, 14 May 2010 11:46:52 +0200

s3d (0.2.1.1-4) unstable; urgency=low

  * Register HTML documentation in doc-base
  * debian/patches:
    - Add libgps_2_90.patch, Fix FTBFS with libgps-2.90 (Closes:
      #560077)

 -- Sven Eckelmann <sven@narfation.org>  Tue, 08 Dec 2009 21:34:58 +0100

s3d (0.2.1.1-3) unstable; urgency=low

  * Remove unused libraries added by pkg-config from libs3d and libs3dw
  * Convert to 3.0 (quilt) source format
  * debian/control:
    - Remove shlibs:Depends for binary packages without shared libs
      dependencies
    - Remove unneeded build dependency to quilt
    - Remove cdbs and change debhelper dependency to 7 for for dh
      support
  * Rewrote debian/rules with dh

 -- Sven Eckelmann <sven@narfation.org>  Mon, 16 Nov 2009 01:32:25 +0100

s3d (0.2.1.1-2) unstable; urgency=low

  * Upgraded to policy 3.8.3, no changes required
  * Add README.source with information about patch management
  * debian/patches:
    - Change to dep3 patch tagging guidelines
    - Remove number before patches as order is given by
      debian/patches/series
    - Add version_number.patch, Correct version number for soname and
      pkg-config
  * Remove Martin Stern from debian/copyright as his files were removed

 -- Sven Eckelmann <sven@narfation.org>  Thu, 15 Oct 2009 18:39:02 +0200

s3d (0.2.1.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Reduce debhelper dependency to 5
    - Only depend on xmlto and docbook-xml in Build-Depends-Indep for
      arch all package s3d-doc
  * debian/patches:
    - Add 100-installable-examples.patch, make example usable without
      extra headers
    - Remove upstream merged patches
      100-hyphen-used-as-minus-sign.patch, 101-fix-sigbus-on-mips.patch,
      102-send-correct-load-over-net.patch, 103-endian-safe-float.patch,
      104-reduce-depends.patch, 105-s3dfm-pathnames.patch,
      106-g3dorientation.patch, 107-fix-cmake-ftbfs.patch,
      108-fix-gpsd-ftbfs.patch, 109-strip-libs3d-parameters.patch,
      110-s3dosm-api06.patch, 111-xmlto-documentation.patch
  * Install examples with libs3d-dev and libs3dw-dev
  * Convert debian/copyright to new dep5 version
  * Upgraded to policy 3.8.2, no changes required
  * Install upstream changelogs

 -- Sven Eckelmann <sven@narfation.org>  Sat, 11 Jul 2009 17:00:52 +0200

s3d (0.2.1-14) unstable; urgency=low

  * Add function and structure manpages for libs3d2 and libs3dw
  * debian/patches:
    - Add 111-xmlto-documentation.patch, workaround for LP: #372243 by
      using xmlto instead of docbook-utils for documentation generation
  * debian/control:
    - Depend on xmlto as new tool to generate the html documentation
    - Update Depend on >=docbook-xml-4.5 for DocBook 4.5 DTDs

 -- Sven Eckelmann <sven@narfation.org>  Mon, 11 May 2009 23:05:11 +0200

s3d (0.2.1-13) unstable; urgency=low

  * debian/patches:
    - Add 109-strip-libs3d-parameters.patch, remove only libs3d options
      from program arguments without looking at the order of occurrence
    - Add 110-s3dosm-api06.patch, use API version 0.6 of
      Openstreetmaps.org
  * debian/control:
    - Depend on docbook-xml to disable DTD lookup by docbook2html on
      www.oasis-open.org (Closes LP: #368537)
    - Allow Debian Maintainers to upload new versions of s3d

 -- Sven Eckelmann <sven@narfation.org>  Tue, 28 Apr 2009 13:43:34 +0200

s3d (0.2.1-12) unstable; urgency=low

  * debian/patches/:
    - Update 104-reduce-depends.patch to apply without warnings
    - Add 108-fix-gpsd-ftbfs.patch to fix ftbfs with libgps-dev 2.39
      (Closes: #524088)

 -- Sven Eckelmann <sven@narfation.org>  Tue, 14 Apr 2009 23:04:41 +0200

s3d (0.2.1-11) unstable; urgency=low

  * debian/rules: Don't install TODO file with every package

 -- Sven Eckelmann <sven@narfation.org>  Wed, 18 Mar 2009 14:14:38 +0100

s3d (0.2.1-10) unstable; urgency=low

  * Rewrote debian/rules using cdbs
  * Move s3d-dbg to new section debug
  * Upgraded to policy 3.8.1, no changes required
  * Add 107-fix-cmake-ftbfs.patch to fix FTBFS with cmake 2.6.3
    (Closes: #520163)

 -- Sven Eckelmann <sven@narfation.org>  Tue, 17 Mar 2009 22:24:56 +0100

s3d (0.2.1-9) unstable; urgency=low

  * debian/rules: Use CFLAGS and LDFLAGS from dpkg-buildpackage
  * Update debian/watch to find .tar.gz
  * debian/control:
    - Correct short description of libs3dw2
    - Only depend on at least one binary package for s3d-dbg
  * Add 106-g3dorientation.patch, correct model orientation with
    libg3d-0.0.7
  * Update copyright years in debian/copyright

 -- Sven Eckelmann <sven@narfation.org>  Sun, 15 Feb 2009 13:29:04 +0100

s3d (0.2.1-8) unstable; urgency=low

  * Move VCS-Git repository to berlios in debian/control
  * Workaround for GCC bug with disappearing minimized dot_mcp programs
    in patches/103-endian-safe-float.patch
  * Add patch 105-s3dfm-pathnames to correct detection of filetypes in
    subpaths in s3dfm
  * Replace deprecated dh_clean -k with dh_prep in debian/rules

 -- Sven Eckelmann <sven@narfation.org>  Wed, 24 Dec 2008 20:58:35 +0100

s3d (0.2.1-7) unstable; urgency=low

  * Replace s3d@packages.debian.org maintainer with first Uploader in
    debian/control

 -- Sven Eckelmann <sven@narfation.org>  Thu, 11 Dec 2008 21:58:44 +0100

s3d (0.2.1-6) unstable; urgency=low

  * Update 103-endian-safe-float.patch to fix memory corruption
    in obj_scale of libs3d
  * Add 104-reduce-depends.patch to reduce number of binary dependencies
  * Correct spelling of OLSR and B.A.T.M.A.N. in descriptions

 -- Sven Eckelmann <sven@narfation.org>  Thu, 27 Nov 2008 21:15:22 +0100

s3d (0.2.1-5) unstable; urgency=low

  * Add separate copyright notice for debian/* in debian/copyright
  * Cleanup comments in debian/rules
  * Don't remove *-stamps in clean target (dh_clean does that)
  * debian/copyright:
    - Add separate copyright notice for all subdirectories
    - Add missing copyright holders
    - Use repeatable Copyright fields as suggested by newer
      CopyrightFormat

 -- Sven Eckelmann <sven@narfation.org>  Wed, 29 Oct 2008 00:42:41 +0100

s3d (0.2.1-4) unstable; urgency=low

  * Include /usr/share/quilt/quilt.make in debian/rules to manage
    patches and add quilt (>= 0.40) to Build-Depends.
  * Fix sigbus on machines without unaligned reads
  * Send correct S3D_P_C_LOAD_* instead  S3D_PC_PEP_* in s3d_load_*
    functions
  * Fix wrong float handling with different endian client+server

 -- Sven Eckelmann <sven@narfation.org>  Sun, 21 Sep 2008 00:50:21 +0200

s3d (0.2.1-3) unstable; urgency=low

  * Use debian policy feature to break uploaders list
  * Remove useless dh_install(examples|mans) from rules

 -- Sven Eckelmann <sven@narfation.org>  Sat, 13 Sep 2008 11:51:14 +0200

s3d (0.2.1-2) unstable; urgency=low

  * Don't use hyphens as minus signs in manpages
  * Use dpkg-gensymbols
  * Add missing newlines in *.install files

 -- Sven Eckelmann <sven@narfation.org>  Fri, 29 Aug 2008 18:54:08 +0200

s3d (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #506869)

 -- Sven Eckelmann <sven@narfation.org>  Wed, 27 Aug 2008 20:40:38 +0200
